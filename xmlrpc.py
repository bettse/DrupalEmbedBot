# http://github.com/bmc/picoblog/blob?path[]=xmlrpc.py&raw=true

# -----------------------------------------------------------------------------
# Imports
# -----------------------------------------------------------------------------

import sys
import xmlrpclib
import logging
import string

from google.appengine.api import urlfetch
from google.appengine.api.urlfetch import DownloadError

# -----------------------------------------------------------------------------
# Classes
# -----------------------------------------------------------------------------

class GoogleXMLRPCTransport(object):
    """Handles an HTTP transaction to an XML-RPC server."""

    def __init__(self):
        self._extra_headers = {}
        pass

#http://svn.python.org/projects/python/trunk/Lib/xmlrpclib.py
    def get_host_info(self, host):
        import urllib
        auth, host = urllib.splituser(host)
        extra_headers = {}
        if auth:
            import base64
            auth = base64.encodestring(urllib.unquote(auth))
            auth = string.join(string.split(auth), "") # get rid of whitespace
            extra_headers = {"Authorization": "Basic " + auth }

        return host, extra_headers

    def request(self, host, handler, request_body, verbose=0):
        """
        Send a complete request, and parse the response. See xmlrpclib.py.

        :Parameters:
            host : str
                target host
                
            handler : str
                RPC handler on server (i.e., path to handler)
                
            request_body : str
                XML-RPC request body
                
            verbose : bool/int
                debugging flag. Ignored by this implementation

        :rtype: dict
        :return: parsed response, as key/value pairs
        """

        # issue XML-RPC request

        result = None
        chost, extra_headers = self.get_host_info(host)
        extra_headers.update({'Content-Type': 'text/xml'})
        url = 'http://%s%s' % (chost, handler)
        #try:
        response = urlfetch.fetch(url=url,
                                  payload=request_body,
                                  method=urlfetch.POST,
                                  headers=extra_headers
                                  )
        #except:
        #    msg = 'Failed to fetch %s' % url
        #    raise xmlrpclib.ProtocolError(chost + handler, 500, msg, {})
        
        if response.status_code != 200:
            logging.error('%s returned status code %s' % 
                          (url, response.status_code))
            raise xmlrpclib.ProtocolError(chost + handler,
                                          response.status_code,
                                          "",
                                          response.headers)
        else:
            result = self.__parse_response(response.content)
        
        return result

    def __parse_response(self, response_body):
        p, u = xmlrpclib.getparser(use_datetime=0)
        p.feed(response_body)
        return u.close()
