from waveapi import events
from waveapi import robot
from waveapi import util

import logging
import xmlrpc
import xmlrpclib
import base64
import time
from ConfigParser import ConfigParser


# Globals
ROBOT_NAME = None
XMLRPC_URL = None
USER = None
PASS = None
node_type = None 
access_set = set()
#TODO Web form on robot add that allows specifying node_type, published status, and if this is a menu element
#TODO access list (set of accepted users diff set of wave members is non empty)


def OnRobotAdded(properties, context):
  """Invoked when the robot has been added."""
  logging.info("Robot Added")
  #Get housekeeping data
  root_wavelet = context.GetRootWavelet()
  doc = root_wavelet.CreateBlip().GetDocument()
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.LABEL, 'xmlrpc_label', 'XMLRPC URL (without "http://")'))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.INPUT, 'xmlrpc_input', XMLRPC_URL, XMLRPC_URL))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.LABEL, 'user_label', 'Username'))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.INPUT, 'user_input', USER, USER))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.LABEL, 'pass_label', 'Password'))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.INPUT, 'pass_input'))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.LABEL, 'nodetype_label', 'Node Type(story, blog, page)'))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.INPUT, 'nodetype_input', node_type, node_type))
  doc.AppendElement(document.FormElement(document.ELEMENT_TYPE.BUTTON,'btn_update', 'Post'))



def BuildHTMLDictionary(properties, context):
 result = {}
 blip = context.GetBlipById(properties['blipId'])
 for key, elem in blip.GetElements().iteritems():
   if elem.type != document.ELEMENT_TYPE.LABEL:
     value = elem.value
     name = elem.name
     result.update({name:value})
 return result


def OnFormButton(properties, context):
  global XMLRPC_URL, USER, PASS, node_type, access_set
  #Parse paramters and assign to relevant variables
  dict = BuildHTMLDictionary(properties, context)
  node_type = dict['nodetype_input']
  XMLRPC_URL = dict['xmlrpc_input']
  USER= dict['user_input']
  PASS = dict['pass_input']
  #Get housekeeping data
  blip = context.GetBlipById(properties['blipId'])
  root_wavelet = context.GetRootWavelet()
  wave_title = root_wavelet.GetTitle()
  waveid = root_wavelet.GetWaveId()
  participant_set = set(root_wavelet.GetParticipants())
  #Delete the form blip
  blip.Delete()
  #Only post if a person with access is on the wave and if the wave has never been posted before
  #if (not root_wavelet.GetDataDocument('embeded_url')) and (len(access_set.union(participant_set)) != 0):
  if (not root_wavelet.GetDataDocument('embeded_url')):
    #add public user
    root_wavelet.AddParticipant('public@a.gwave.com')
    logging.info("Adding public participant")
    #Embed in new drupal page
    transport = xmlrpc.GoogleXMLRPCTransport()
    server = xmlrpclib.ServerProxy("http://%s:%s@%s" % (USER, PASS, XMLRPC_URL), transport=transport, verbose=1) 
    post = { 'title': wave_title, 'description': '<div id="wave" style="width: 100%%; height: 600px">%s</div>' % waveid}
    post_id = server.metaWeblog.newPost(node_type, USER, PASS, post, xmlrpclib.Boolean(True))
    logging.info("id is %s" % post_id)
    root_wavelet.CreateBlip().GetDocument().SetText("This wave embedded at: http://ericbetts.org/node/%s" % post_id)
    root_wavelet.SetDataDocument('embeded_url', "http://ericbetts.org/node/%s" % post_id)


if __name__ == '__main__':
  global XMLRPC_URL, USER, PASS, node_type
  config = ConfigParser()
  config.read("config.ini")
  ROBOT_NAME = config.get('bot', 'name')
  XMLRPC_URL = config.get('bot', 'url')
  USER = config.get('bot', 'user')
  PASS = config.get('bot', 'pass')
  node_type = config.get('bot', 'node_type')
  access_set = set(config.get('bot', 'access_list').split(','))

  myRobot = robot.Robot(ROBOT_NAME.capitalize(),
      image_url='http://' + ROBOT_NAME + '.appspot.com/icon.jpg',
      version='1',
      profile_url='http://github.com/bettse/DrupalEmbedBot')

  #logging.debug("Robot Initilaized")
  myRobot.RegisterHandler(events.WAVELET_SELF_ADDED, OnRobotAdded)
  myRobot.RegisterHandler(events.FORM_BUTTON_CLICKED, OnFormButton)
  myRobot.Run()


